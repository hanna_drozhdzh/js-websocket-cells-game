import Observable from './Observable';

class AppModel extends Observable {
  #dialog;

  #clientId;
  #gameId;
  #playerColor;

  #balls;
  #clients;
  #games;

  constructor() {
    super();

    this.dialog = true;
    this.clients = [];
    this.games = {};
  }

  get dialog() {
    return this.#dialog;
  }

  set dialog(value) {
    this.#dialog = value;
  }

  get clientId() {
    return this.#clientId;
  }

  set clientId(id) {
    this.#clientId = id;
  }

  get playerColor() {
    return this.#playerColor;
  }

  set playerColor(color) {
    this.#playerColor = color;
  }

  get gameId() {
    return this.#gameId;
  }

  set gameId(id) {
    this.#gameId = id;
  }

  get balls() {
    return this.#balls;
  }

  set balls(value) {
    this.#balls = value;
  }

  get clients() {
    return [...this.#clients];
  }

  set clients(values) {
    this.#clients = [...values];
  }

  get games() {
    return { ...this.#games };
  }

  set games(values) {
    this.#games = { ...values };
  }
}

export default AppModel;
