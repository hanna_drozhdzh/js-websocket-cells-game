import AppActions from '../types/AppActions';

class AppController {
  constructor(model) {
    this.model = model;

    this.ws = new WebSocket('ws://localhost:9090');
    this.ws.onmessage = this.onmessage.bind(this);
  }

  handleEvent(e) {
    e.stopPropagation();

    switch (e.type) {
      case 'click':
        this.clickHandler(e.target);
        break;
      case 'change':
        this.changeHandler(e.target);
        break;
      default:
        console.log(e.target);
    }
  }

  clickHandler(target) {
    // skip all non control elements, for click event non <button>
    if (
      !target.getAttribute('id') ||
      target.tagName.toLowerCase() !== 'button'
    ) {
      return;
    }

    console.info(target.getAttribute('id'));

    if (target.getAttribute('id') === 'new-game-btn') {
      this.createGame();
    }

    if (target.getAttribute('id') === 'join-game-btn') {
      this.joinGame();
    }

    if (target.getAttribute('id').includes('ball')) {
      this.doPlayerMove(target);
    }
  }

  changeHandler(target) {
    // skip all non control elements, for change event non <input>
    if (
      !target.getAttribute('id') ||
      target.tagName.toLowerCase() !== 'input'
    ) {
      return;
    }

    if (target.getAttribute('id') === 'input-game-id') {
      this.model.gameId = target.value;
      this.model.notify({
        action: AppActions.UPDATE_INFO_GAME_ID,
        model: this.model,
      });
    }

    if (target.getAttribute('id') === 'input-balls') {
      this.model.balls = target.value;
      this.model.notify({
        action: AppActions.UPDATE_INFO_BALLS,
        model: this.model,
      });
    }
  }

  createGame() {
    const balls =
      this.model.balls ?? document.querySelector(`#input-balls`).value; // prevent async competition

    const payLoad = {
      method: 'create',
      clientId: this.model.clientId,
      balls,
    };

    this.ws.send(JSON.stringify(payLoad));
  }

  joinGame() {
    const gameId =
      this.model.gameId ?? document.querySelector(`#input-game-id`).value; // prevent async competition

    const payLoad = {
      method: 'join',
      clientId: this.model.clientId,
      gameId,
    };

    this.ws.send(JSON.stringify(payLoad));
  }

  doPlayerMove(target) {
    console.info(this.model.playerColor, target);

    const payLoad = {
      method: 'play',
      clientId: this.model.clientId,
      gameId: this.model.gameId,
      ballId: target.getAttribute('id'),
      color: this.model.playerColor,
    };
    this.ws.send(JSON.stringify(payLoad));
  }

  onmessage(message) {
    // message.data
    const response = JSON.parse(message.data);

    // connect
    if (response.method === 'connect') {
      this.model.clientId = response.clientId;
      this.model.games = response.games;
      this.model.notify({
        action: AppActions.UPDATE_INFO_CLIENT_ID,
        model: this.model,
      });

      console.log('Client id set successfully ' + this.model.clientId);
    }

    // create
    if (response.method === 'create') {
      this.model.gameId = response.game.id;
      this.model.notify({
        action: AppActions.UPDATE_INFO_GAME_ID,
        model: this.model,
      });

      console.info(
        `Created game with id ${this.model.gameId} with ${response.game.balls}`
      );
    }

    // join
    if (response.method === 'join') {
      this.model.gameId = response.game.id;
      this.model.clients = response.game.clients;
      this.model.balls = response.game.balls;

      this.model.playerColor = response.game.clients.find(
        (client) => client.clientId === this.model.clientId
      ).color;

      this.model.dialog = false;

      this.model.notify({
        action: AppActions.JOIN_GAME,
        model: this.model,
      });
    }

    // update
    if (response.method === 'update') {
      if (!response.game.state) {
        return;
      }

      for (const b of Object.keys(response.game.state)) {
        const color = response.game.state[b];

        this.model.notify({
          action: AppActions.DO_PLAYER_MOVE,
          model: {
            self: this.model,
            id: b,
            color,
          },
        });
      }
    }

    // update-game-list
    if (response.method === 'update-game-list') {
      if (!response.games) {
        return;
      }

      this.model.games = response.games;
      this.model.notify({
        action: AppActions.UPDATE_GAMES,
        model: this.model,
      });
    }
  }
}

export default AppController;
